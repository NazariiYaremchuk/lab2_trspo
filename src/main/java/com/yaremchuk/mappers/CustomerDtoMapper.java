package com.yaremchuk.mappers;

import  com.yaremchuk.dtos.customer.CustomerDto;
import com.yaremchuk.entities.Customer;
import com.yaremchuk.entities.Rent;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class CustomerDtoMapper extends AbstractConverter<Customer, CustomerDto> {
    @Override
    protected CustomerDto convert(Customer customer) {
        return CustomerDto.builder()
                .id(customer.getId())
                .name(customer.getName())
                .surmane(customer.getSurname())
                .rents(customer.getRents().stream()
                        .map(Rent::toString)
                        .collect(Collectors.toList()))
                .build();
    }
}
