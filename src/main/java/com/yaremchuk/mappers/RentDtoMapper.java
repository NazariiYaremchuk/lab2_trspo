package com.yaremchuk.mappers;

import com.yaremchuk.dtos.rent.RentDto;
import com.yaremchuk.entities.Rent;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class RentDtoMapper extends AbstractConverter<Rent, RentDto> {
    @Override
    protected RentDto convert(Rent rent) {
        return RentDto.builder()
                .end(rent.getEndOfRent())
                .start(rent.getStartOfRent())
                .build();
    }
}