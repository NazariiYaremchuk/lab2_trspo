package com.yaremchuk.mappers;

import com.yaremchuk.dtos.car.AddCarDto;
import com.yaremchuk.entities.Car;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class CarMapper extends AbstractConverter<AddCarDto, Car> {
    @Override
    protected Car convert(AddCarDto addCarDto) {
        return Car.builder()
                .code(addCarDto.getCode())
                .name(addCarDto.getName())
                .build();
    }
}
