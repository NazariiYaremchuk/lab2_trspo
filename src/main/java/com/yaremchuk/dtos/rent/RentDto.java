package com.yaremchuk.dtos.rent;

import com.yaremchuk.dtos.car.CarDto;
import com.yaremchuk.dtos.customer.CustomerDto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.sql.Date;

@Setter
@Getter
@Builder
public class RentDto {
    @NotEmpty
    private Date start;
    @NotEmpty
    private Date end;
    @NotEmpty
    private CarDto car;
    @NotEmpty
    private CustomerDto customerDto;
}
