package com.yaremchuk.services;

import com.yaremchuk.dtos.customer.AddCustomerDto;
import com.yaremchuk.dtos.customer.CustomerDto;
import com.yaremchuk.entities.Customer;

import java.util.List;

public interface CustomerService {
    List<CustomerDto> getAll();

    Customer getById(Long id);

    CustomerDto save(AddCustomerDto addCustomerDto);

    CustomerDto update(AddCustomerDto addCustomerDto, Long id);

    void delete(Long id);
}
