package com.yaremchuk.services;

import com.yaremchuk.dtos.car.AddCarDto;
import com.yaremchuk.dtos.car.CarDto;
import com.yaremchuk.entities.Car;

import java.util.List;

public interface CarService {
    List<CarDto> getAll();

    Car getById(Long id);

    CarDto save(AddCarDto addCarDto);

    CarDto update(AddCarDto addCarDto, Long id);

    void delete(Long id);
}
