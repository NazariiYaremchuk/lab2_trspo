package com.yaremchuk.services;

import com.yaremchuk.dtos.rent.AddRentDto;
import com.yaremchuk.dtos.rent.RentDto;

import java.util.List;

public interface RentService {
    List<RentDto> getAll();

    RentDto save(AddRentDto addRentDto);

    RentDto update(AddRentDto addRentDto, Long id);

    void delete(Long id);
}
