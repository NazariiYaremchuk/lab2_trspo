package com.yaremchuk.repositories;

import com.yaremchuk.entities.Rent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RentRepo extends JpaRepository<Rent, Long> {
}